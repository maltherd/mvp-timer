from setuptools import setup, find_packages


from mvp import __version__

setup(
    name="mvp",
    version=__version__,
    description="Small timer for MVPs",
    author="Arnaud Gaudard",
    author_email="gaudard@protonmail.ch",
    packages=find_packages(),
    include_package_data=True,
    package_data={'mvp': ['res/*']},
    install_requires=[
        'pyqt5',
        'qasync'
    ],
    classifiers=[
    ],
    scripts=[
        'bin/mvp',
    ],
)
