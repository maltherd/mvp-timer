import os
import sys
import asyncio
import json
import time
import signal
from datetime import timedelta
from socket import gethostname

from qasync import QEventLoop, asyncSlot

from PyQt5.QtWidgets import (
    QApplication, QWidget, QTableWidget, QTableWidgetItem, QLabel, QLineEdit, QPushButton, QVBoxLayout, QHBoxLayout, QHeaderView, QAbstractItemView, QFrame)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSignal, QObject, Qt, QSize


SCORE_TIME_S = 5
ROUND_TIME_S = 10

DEFAULT_PORT = 45654
NETWORK_FREQ = 1/150

RES_PATH = os.path.join(os.path.dirname(__file__), 'res')


class Client(QObject):
    ratingMade = pyqtSignal('PyQt_PyObject', 'PyQt_PyObject')
    orderingReceived = pyqtSignal('PyQt_PyObject')
    roundReceived = pyqtSignal('PyQt_PyObject')

    def __init__(self, parent):
        super().__init__(parent)
        self.username = None
        self.rd = None
        self.wr = None
        self.partialMessage = b''
        self.handleTask = None

    async def connect(self, ip, port=DEFAULT_PORT, username=""):

        if self.rd or self.wr:
            self.wr.close()
            await self.wr.wait_closed()
            self.handleTask.cancel()
            self.rd = None
            self.wr = None
            self.partialMessage = b''

        self.username = username if username else gethostname()
        future = asyncio.open_connection(ip, port)

        try:
            self.rd, self.wr = await asyncio.wait_for(future, timeout=5)
            self.handleTask = asyncio.create_task(self.handleMessages())
        except asyncio.TimeoutError:
            raise asyncio.TimeoutError('Timeout')

    @asyncSlot()
    async def call(self):
        now = time.monotonic()
        response = {'cmd': 'CALL', 'now': now, 'who': self.username}
        self.wr.write("{};".format(json.dumps(response)).encode('utf-8'))
        await self.wr.drain()

    @asyncSlot('PyQt_PyObject')
    async def rate(self, value):
        response = {'cmd': 'RATE', 'value': value, 'who': self.username}
        self.wr.write("{};".format(json.dumps(response)).encode('utf-8'))
        await self.wr.drain()

    async def handleMessages(self):

        while True:
            try:
                data = await self.rd.readuntil(b';')
                data = '{}{}'.format(self.partialMessage.decode('utf-8'), data.decode('utf-8'))[:-1]  # remove ';'
                self.partialMessage = bytes()

                data = json.loads(data)
                reaction = {
                    'PING': self.handlePing,
                    'RATE': self.handleRate,
                    'ORDER': self.handleOrder,
                    'ROUND': self.handleRound,
                }
                await reaction.get(data['cmd'])(data)

            except asyncio.IncompleteReadError as ire:
                self.partialMessage = ire.partial

            except (BrokenPipeError, ConnectionResetError):
                break

            if self.wr.is_closing():
                break

            await asyncio.sleep(NETWORK_FREQ)

    async def handlePing(self, data):
        now = time.monotonic()
        response = {'cmd': 'PONG', 'now': now, 'serial': data['serial']}
        self.wr.write("{};".format(json.dumps(response)).encode('utf-8'))
        await self.wr.drain()

    async def handleRate(self, data):
        self.ratingMade.emit(data['stack'], data['show'])

    async def handleOrder(self, data):
        self.orderingReceived.emit(data['order'])

    async def handleRound(self, data):
        self.roundReceived.emit(data['counter'])


class MainWindow(QWidget):
    """Main window."""

    def __init__(self):
        super().__init__()

        self.client = Client(self)
        self.client.orderingReceived.connect(lambda ordering: self.orderingReceived(ordering))
        self.client.roundReceived.connect(lambda counter: self.roundReceived(counter))
        self.client.ratingMade.connect(lambda stack, show: self.ratingMade(stack, show))

        self.board_lock = asyncio.Lock()
        self.ratings_lock = asyncio.Lock()

        self.gameStatusTask = None

        # UI

        self.setLayout(QVBoxLayout())

        # Server Bar

        q = QWidget(self)

        self.username = QLineEdit(q)
        self.username.setPlaceholderText('Username')

        self.serverIp = QLineEdit(q)
        self.serverIp.setPlaceholderText('69.69.69.69')
        self.serverIp.setText('127.0.0.1')

        self.serverConnect = QPushButton('Connect', q)
        self.serverConnect.clicked.connect(self.connectToServer)

        self.serverStatus = QLabel('Idle', q)

        q.setLayout(QHBoxLayout())
        q.layout().addWidget(self.username)
        q.layout().addWidget(self.serverIp)
        q.layout().addWidget(self.serverConnect)
        q.layout().addWidget(self.serverStatus)

        self.layout().addWidget(q)

        #

        q = QFrame(self)
        q.setFrameShape(QFrame.HLine)
        q.setFrameShadow(QFrame.Sunken)
        self.layout().addWidget(q)

        # Leader board
        self.gameStatus = QLabel('')
        self.gameStatus.setAlignment(Qt.AlignCenter)
        self.gameStatus.setStyleSheet('font-size: 18pt; color: gray;')
        self.layout().addWidget(self.gameStatus)

        q = QWidget(self)

        self.board = QTableWidget(0, 3, self)
        self.board.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.board.horizontalHeader().hide()
        self.board.verticalHeader().hide()
        self.board.horizontalHeader().setStretchLastSection(True)
        self.board.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.board.verticalHeader().setStretchLastSection(True)
        self.board.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.board.setSelectionMode(QAbstractItemView.NoSelection)

        self.ratings = QTableWidget(2, 1, self)
        self.ratings.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.ratings.horizontalHeader().hide()
        self.ratings.verticalHeader().hide()
        self.ratings.horizontalHeader().setStretchLastSection(True)
        self.ratings.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ratings.verticalHeader().setStretchLastSection(True)
        self.ratings.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ratings.setSelectionMode(QAbstractItemView.NoSelection)

        self.ratings.setItem(0, 0, QTableWidgetItem(QIcon('{}/thumbs-up.svg'.format(RES_PATH)), ''))
        self.ratings.setItem(1, 0, QTableWidgetItem(QIcon('{}/thumbs-down.svg'.format(RES_PATH)), ''))

        q.setLayout(QHBoxLayout())
        q.layout().addWidget(self.board)
        q.layout().addWidget(self.ratings)

        self.layout().addWidget(q)

        #

        q = QFrame(self)
        q.setFrameShape(QFrame.HLine)
        q.setFrameShadow(QFrame.Sunken)
        self.layout().addWidget(q)

        # Buttons bar

        q = QWidget(self)

        self.callButton = QPushButton(QIcon('{}/volume-2.svg'.format(RES_PATH)), '', q)
        self.callButton.clicked.connect(self.call)
        self.callButton.setMinimumHeight(100)
        self.callButton.setIconSize(QSize(75, 75))

        def rateButtonLogic(value):
            if value == 'GOOD':
                if self.trashButton.isChecked():
                    self.trashButton.setChecked(False)

                if self.goodButton.isChecked():
                    self.rate('GOOD')
                    return

            elif value == 'TRASH':
                if self.goodButton.isChecked():
                    self.goodButton.setChecked(False)

                if self.trashButton.isChecked():
                    self.rate('TRASH')
                    return

            self.rate('NOTHING')

        self.goodButton = QPushButton(QIcon('{}/thumbs-up.svg'.format(RES_PATH)), '', q)
        self.goodButton.setCheckable(True)
        self.goodButton.clicked.connect(lambda: rateButtonLogic('GOOD'))
        self.goodButton.setFixedSize(QSize(100, 100))
        self.goodButton.setIconSize(QSize(75, 75))

        self.trashButton = QPushButton(QIcon('{}/thumbs-down.svg'.format(RES_PATH)), '', q)
        self.trashButton.setCheckable(True)
        self.trashButton.clicked.connect(lambda: rateButtonLogic('TRASH'))
        self.trashButton.setFixedSize(QSize(100, 100))
        self.trashButton.setIconSize(QSize(75, 75))

        q.setLayout(QHBoxLayout())
        q.layout().addWidget(self.callButton)
        # q.layout().addStretch(1)
        q.layout().addWidget(self.goodButton)
        q.layout().addWidget(self.trashButton)

        self.layout().addWidget(q)

    @asyncSlot()
    async def orderingReceived(self, ordering):

        async def newRound():
            await asyncio.sleep(SCORE_TIME_S)
            self.callButton.setEnabled(True)
            self.gameStatus.setText('Ready for a new round.')

        def ordinal(n):
            last_digit = n % 10
            special_suffixes_by_last_digit = {
                1: 'st',
                2: 'nd',
                3: 'rd',
            }
            special_suffixes_by_number = {
                11: 'th',
                12: 'th',
                13: 'th',
            }

            if n in special_suffixes_by_number:
                suffix = special_suffixes_by_number.get(last_digit)
            elif last_digit in special_suffixes_by_last_digit:
                suffix = special_suffixes_by_last_digit.get(last_digit)
            else:
                suffix = 'th'

            return '{}{}'.format(n, suffix)

        self.gameStatus.setText('Round over !')
        self.callButton.setEnabled(False)

        async with self.board_lock:
            self.board.setRowCount(0)

        sorted_ordering = sorted([dictionary for who, dictionary in ordering.items()], key=lambda dictionary: dictionary['timestamp'])
        first = sorted_ordering[0]

        async with self.board_lock:
            for index, dictionary in enumerate(sorted_ordering):

                delta = timedelta(seconds=dictionary['timestamp'] - first['timestamp'])

                self.board.insertRow(index)

                self.board.setItem(index, 0, QTableWidgetItem(ordinal(index + 1)))
                self.board.setItem(index, 1, QTableWidgetItem(dictionary['who'] if dictionary['who'] != first['who'] else '{} (MVP)'.format(dictionary['who'])))

                if dictionary['timestamp'] == first['timestamp']:
                    self.board.setItem(index, 2, QTableWidgetItem(''))
                else:
                    self.board.setItem(index, 2, QTableWidgetItem('+{}s{}'.format(delta.seconds, int(delta.microseconds/1000))))

                self.board.item(index, 0).setTextAlignment(Qt.AlignCenter)
                self.board.item(index, 1).setTextAlignment(Qt.AlignCenter)
                self.board.item(index, 2).setTextAlignment(Qt.AlignCenter)

        asyncio.create_task(newRound())

    @asyncSlot()
    async def roundReceived(self, counter):

        async def updateStatus():
            seconds_left = ROUND_TIME_S
            while seconds_left != 0:
                self.gameStatus.setText('Round {} running ! ({}s left)'.format(counter, seconds_left))
                await asyncio.sleep(1)
                seconds_left -= 1

        def resetRatings():
            self.goodButton.setChecked(False)
            self.trashButton.setChecked(False)
            self.rate('NOTHING')

        self.gameStatusTask = asyncio.create_task(updateStatus())
        resetRatings()

    @asyncSlot()
    async def ratingMade(self, stack, show):

        if show:
            good_voters = []
            trash_voters = []

            for dictionary in stack.values():
                lists = {
                    'GOOD': good_voters,
                    'TRASH': trash_voters,
                }
                lists.get(dictionary['rating']).append(dictionary['who'])

            self.ratings.item(0, 0).setText('{}: {}'.format(len(good_voters), ', '.join(good_voters)))
            self.ratings.item(1, 0).setText('{}: {}'.format(len(trash_voters), ', '.join(trash_voters)))

        else:
            if stack:
                self.ratings.item(0, 0).setText('?')
                self.ratings.item(1, 0).setText('?')
            else:
                self.ratings.item(0, 0).setText('')
                self.ratings.item(1, 0).setText('')

    # @asyncClose
    # async def closeEvent(self, event):
    #     await self.session.close()

    @asyncSlot()
    async def connectToServer(self):
        self.serverConnect.setEnabled(False)
        self.serverStatus.setText('Connecting...')
        try:
            await self.client.connect(self.serverIp.text(), username=self.username.text())
            self.callButton.setEnabled(True)

            if self.gameStatusTask:
                self.gameStatusTask.cancel()

            self.gameStatus.setText('')
            self.serverStatus.setText('Connected')

        except Exception as exc:
            self.serverStatus.setText('Error: {}'.format(exc))
        finally:
            self.serverConnect.setEnabled(True)

    @asyncSlot()
    async def call(self):
        self.callButton.setEnabled(False)
        try:
            await self.client.call()
        except Exception as exc:
            self.serverStatus.setText('Error: {}'.format(exc))
            self.callButton.setEnabled(True)
        finally:
            pass  # button is re-enabled later, after ORDER received

    @asyncSlot('PyQt_PyObject')
    async def rate(self, value):
        try:
            await self.client.rate(value)
        except Exception as exc:
            self.serverStatus.setText('Error: {}'.format(exc))


app = QApplication(sys.argv)
loop = QEventLoop(app)
asyncio.set_event_loop(loop)
signal.signal(signal.SIGINT, signal.SIG_DFL)

mainWindow = MainWindow()
mainWindow.show()

with loop:
    sys.exit(loop.run_forever())
