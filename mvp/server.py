import asyncio
import uuid
import json
import time
from collections import defaultdict, deque

# Game Stuff

ROUND_TIME_S = 10
SCORE_TIME_S = 5

call_state_lock = asyncio.Lock()
call_state = 'IDLE'
call_stack = {}

rating_state_lock = asyncio.Lock()
rating_state = 'TALLYING'
rating_stack = {}

round_counter = 1

# Network Stuff

DEFAULT_PORT = 45654
NETWORK_FREQ = 1/150
PING_FREQ = 1
MISSED_PINGS_LIMIT = 8
LATENCY_HISTORY_LENGTH = 30

clients = {}
network_state = defaultdict(lambda: defaultdict(lambda: None))
latency_history = defaultdict(lambda: deque(maxlen=LATENCY_HISTORY_LENGTH))
partialMessages = defaultdict(lambda: bytes())


def average_latency(client_id):
    return sum(latency_history[client_id])/len(latency_history[client_id])


async def trySend(client_id, response):
    try:
        rd, wr = clients[client_id]

        print('({})[{}] <- {}'.format(call_state, client_id, response['cmd']))
        wr.write("{};".format(json.dumps(response)).encode('utf-8'))
        await wr.drain()

    except (ConnectionResetError, BrokenPipeError):
        pass


async def newClient(rd, wr):
    client_id = uuid.uuid4()
    print('[{}] new client'.format(client_id))
    clients[client_id] = (rd, wr)

    async def maintainClient():
        hm = asyncio.create_task(handleMessages(client_id))

        await asyncio.create_task(maintainPing(client_id))
        wr.close()
        try:
            await wr.wait_closed()
        except BrokenPipeError:
            pass
        await asyncio.wait([hm])

        del clients[client_id]
        del network_state[client_id]
        del latency_history[client_id]
        del partialMessages[client_id]
        if client_id in rating_stack:
            del rating_stack[client_id]

        print('[{}] deleted client'.format(client_id))

    asyncio.create_task(maintainClient())


async def maintainPing(client_id):
    rd, wr = clients[client_id]

    while True:
        if not network_state[client_id]['last_ping_serial']:
            ping_serial = str(uuid.uuid4())

            response = {'cmd': 'PING', 'serial': ping_serial}
            await trySend(client_id, response)

            now = time.monotonic()
            network_state[client_id]['last_ping_serial'] = ping_serial
            network_state[client_id]['last_ping_time'] = now
            network_state[client_id]['missed_pings'] = 0
        else:
            network_state[client_id]['missed_pings'] += 1

        if network_state[client_id]['missed_pings'] > MISSED_PINGS_LIMIT:
            break

        await asyncio.sleep(PING_FREQ)


async def handleMessages(client_id):
    rd, wr = clients[client_id]

    while True:
        try:
            data = await rd.readuntil(b';')
            data = '{}{}'.format(partialMessages[client_id].decode('utf-8'), data.decode('utf-8'))[:-1]  # remove ';'
            partialMessages[client_id] = bytes()

            data = json.loads(data)
            reaction = {
                'PONG': handlePong,
                'CALL': handleCall,
                'RATE': handleRate,
            }
            print('({})[{}] -> {}'.format(call_state, client_id, data['cmd']))
            await reaction.get(data['cmd'])(client_id, data)

        except asyncio.IncompleteReadError as ire:
            partialMessages[client_id] = ire.partial

        except (BrokenPipeError, ConnectionResetError):
            break

        if wr.is_closing():
            break

        await asyncio.sleep(NETWORK_FREQ)


async def handlePong(client_id, data):
    now = time.monotonic()

    if network_state[client_id]['last_ping_serial'] != data['serial']:
        print('belgian internet')
        return

    rtt = now - network_state[client_id]['last_ping_time']
    latency_history[client_id].append(rtt/2)

    # clock desync allows to know by how much the client and server clocks are advancing/late compared to each other.
    # when a client sends its time stamp, we can use it to know at which server time that corresponds, approximately.
    network_state[client_id]['clock_desync'] = now - (data['now'] + average_latency(client_id))
    network_state[client_id]['last_ping_serial'] = ''


async def handleCall(client_id, data):

    global call_state

    async def stateRotation():
        global call_state
        global call_stack

        await asyncio.sleep(ROUND_TIME_S)
        async with call_state_lock:
            call_state = 'SCORE'
            await sendOrders()

        await asyncio.sleep(SCORE_TIME_S)
        async with call_state_lock:
            call_state = 'IDLE'
            call_stack.clear()

    async def sendOrders():
        now = time.monotonic()

        # copy not strictly needed since this is called in an await, inside a lock => no risk of data race. just here for good measure (and converting uuid to str)
        call_stack_copy = {str(id): v for id, v in call_stack.items()}

        for client_id in clients:
            response = {
                'cmd': 'ORDER',
                'now': now,
                'order': call_stack_copy,
            }
            await trySend(client_id, response)

    async def sendRounds():
        global round_counter

        now = time.monotonic()

        for client_id in clients:
            response = {
                'cmd': 'ROUND',
                'now': now,
                'counter': round_counter,
            }
            await trySend(client_id, response)

        round_counter += 1

    async with call_state_lock:
        if call_state == 'IDLE':
            call_state = 'ROUND'
            await sendRounds()
            asyncio.create_task(stateRotation())

        if call_state == 'ROUND':
            # Register CALL
            server_time_when_call_was_made = data['now'] + network_state[client_id]['clock_desync']
            call_stack[client_id] = {'timestamp': server_time_when_call_was_made, 'who': data['who']}

        if call_state == 'SCORE':
            # Ignore CALL.
            pass


async def handleRate(client_id, data):

    async with rating_state_lock:
        if data['value'] == 'NOTHING':
            if client_id in rating_stack:
                del rating_stack[client_id]
        else:
            rating_stack[client_id] = {
                'who': data['who'],
                'rating': data['value']
            }

        rating_stack_copy = {str(id): v for id, v in rating_stack.items()}

    for client_id in clients:
        response = {
            'cmd': 'RATE',
            'stack': rating_stack_copy,
            'show': len(rating_stack) == len(clients),
        }
        await trySend(client_id, response)


async def main():
    server = await asyncio.start_server(newClient, port=DEFAULT_PORT)
    addr = server.sockets[0].getsockname()
    print('Serving on {}'.format(addr))

    async with server:
        await server.serve_forever()


asyncio.run(main())
